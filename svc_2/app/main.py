"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"


import json
import logging
from json import JSONDecodeError
from flask import Flask
from flask import request
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

logger = XprLogger("svc_2",level=logging.INFO)

def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/operate', methods=["POST"])
def hello_world():
    """
    Send response to Hello World
    """

    logger.info("Received request from {}".format(request.remote_addr))
    try:
        logger.info("Processing the request")
        logger.info("Request Processing Done")
        if request.method == 'POST' and request.is_json:
            input_request = request.get_json()
        else:
            print('Invalid request.')
            return
        number = int(input_request['number'])
        logger.info("Sending Response to {}".format(request.remote_addr))
        return f'Cube of number {number} : {number ** 3}\n'

    except (FileNotFoundError, JSONDecodeError):
        logger.error("Request Processing Failed")
        logger.info("Sending Default Response")
        return '<html><body><b>Hello World!</b></body></html>'


if __name__ == '__main__':
    app.run(debug=False, port='6666', host='0.0.0.0', use_reloader=False)
